package main

import (
	pb "bitbucket.org/tiluvy/shippy-service-user/proto/user"
	"context"
	micro "github.com/micro/go-micro"
	"github.com/micro/go-micro/broker"
	_ "github.com/micro/go-plugins/broker/nats"
	"log"
)

const topic = "user.created"

type Subscriber struct{}

func (sub *Subscriber) Process(ctx context.Context, user *pb.User) error {
	log.Println("Picked up a new message")
	log.Println("Sending email to:", user.Name)
	return nil
}

func main() {
	srv := micro.NewService(
		micro.Name("shippy.service.email"),
		micro.Version("latest"),
	)

	srv.Init()

	// Get the broker instance using our environment variables
	pubsub := srv.Server().Options().Broker
	if err := pubsub.Connect(); err != nil {
		log.Fatal(err)
	}

	_, err := broker.Subscribe(topic, func(p broker.Event) error {
		log.Println("[sub] received message:", string(p.Message().Body), "header", p.Message().Header)
		return nil
	})
	if err != nil {
		log.Println(err)
	}

	//_, err := pubsub.Subscribe(topic, func(p broker.Publication) error {
	//	var user *pb.User
	//	if err := json.Unmarshal(p.Message().Body, &user); err != nil {
	//		return err
	//	}
	//	log.Println(user)
	//	go sendEmail(user)
	//	return nil
	//})

	if err != nil {
		log.Println(err)
	}

	// Run the server
	if err := srv.Run(); err != nil {
		log.Println(err)
	}

	//micro.RegisterSubscriber(topic, srv.Server(), new(Subscriber))
	//
	//// Run the server
	//if err := srv.Run(); err != nil {
	//	log.Println(err)
	//}
}

func sendEmail(user *pb.User) error {
	log.Println("Sending email to:", user.Name)
	return nil
}